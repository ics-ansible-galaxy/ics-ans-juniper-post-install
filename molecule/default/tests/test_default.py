import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("juniper_post_install")


def test_syslog_remote_host_added(host):
    assert os.system("zcat /config/juniper.conf.gz | grep 172.30.4.43")


def test_syslog_ntp_added(host):
    assert os.system("zcat /config/juniper.conf.gz | grep boot-server")
    assert os.system("zcat /config/juniper.conf.gz | grep 172.30.0.38")


def test_name_servers_added(host):
    assert os.system("zcat /config/juniper.conf.gz | grep name-server")
